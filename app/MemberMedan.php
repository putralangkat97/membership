<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Alfa6661\AutoNumber\AutoNumberTrait;

class MemberMedan extends Model
{
    protected $fillable = [
        'no_anggota',
        'nama',
        'asal_sekolah',
        'jenis_kelamin',
        'tanggal_lahir',
        'alamat',
        'no_telepon',
        'lead_train_satu',
        'lead_train_dua',
        'lead_train_tiga',
        'dibuat_oleh',
    ];
    
    use AutoNumberTrait;

    /**
     * Return the autonumber configuration array for this model.
     *
     * @return array
     */

    public function getAutoNumberOptions()
    {
        return [
            'no_anggota' => [
                'format' => '01'.date("ymd").'?', // autonumber format. '?' will be replaced with the generated number.
                'length' => 3 // The number of digits in an autonumber
            ]
        ];
    }
}
