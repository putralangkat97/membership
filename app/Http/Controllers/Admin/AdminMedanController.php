<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\MemberMedan;
use Validator;
use Auth;

class AdminMedanController extends Controller
{
    public function index()
    {
        $idm = MemberMedan::find('no_anggota');
        $memberMedan = MemberMedan::all();
        return view('admin.dashboard', compact('memberMedan', 'idm'));
    }

    public function insert(Request $request)
    {
        $rules = array(
            'nama'  => 'required',
            'asal_sekolah'  => 'required',
            'jenis_kelamin'  => 'required',
            'tanggal_lahir'  => 'required',
            'no_telepon'  => 'required',
            'alamat'  => 'required',
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $form_data = array(
            'no_anggota' => $request->no_anggota,
            'nama' => $request->nama,
            'asal_sekolah' => $request->asal_sekolah,
            'jenis_kelamin' => $request->jenis_kelamin,
            'tanggal_lahir' => $request->tanggal_lahir,
            'no_telepon' => $request->no_telepon,
            'alamat' => $request->alamat,
            'dibuat_oleh' => $request->dibuat_oleh,
        );

        MemberMedan::create($form_data);

        return response()->json(['success' => 'Data Added']);
    }

    public function update($id)
    {
        if(request()->ajax())
        {
            $data = MemberMedan::findOrFail($id);
            return response()->json(['result' => $data]);
        }
    }
}
