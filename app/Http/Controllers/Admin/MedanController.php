<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MemberMedan;
use Validator;
use Auth;

class MedanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idm = MemberMedan::find('no_anggota');
        $memberMedan = MemberMedan::all();
        return view('admin.dashboard', compact('memberMedan', 'idm'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'nama'  => 'required',
            'asal_sekolah'  => 'required',
            'jenis_kelamin'  => 'required',
            'tanggal_lahir'  => 'required',
            'no_telepon'  => 'required',
            'alamat'  => 'required',
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $form_data = array(
            'no_anggota' => $request->no_anggota,
            'nama' => $request->nama,
            'asal_sekolah' => $request->asal_sekolah,
            'jenis_kelamin' => $request->jenis_kelamin,
            'tanggal_lahir' => $request->tanggal_lahir,
            'no_telepon' => $request->no_telepon,
            'alamat' => $request->alamat,
            'dibuat_oleh' => $request->dibuat_oleh,
        );

        MemberMedan::create($form_data);

        return response()->json(['success' => 'Data Added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(request()->ajax())
        {
            $idm = MemberMedan::find($id);
            return response()->json(['result' => $idm]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MemberMedan $memberMedan)
    {
        $rules = array(
            'nama'  => 'required',
            'asal_sekolah'  => 'required',
            'jenis_kelamin'  => 'required',
            'tanggal_lahir'  => 'required',
            'no_telepon'  => 'required',
            'alamat'  => 'required',
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $form_data = array(
            'no_anggota' => $request->no_anggota,
            'nama' => $request->nama,
            'asal_sekolah' => $request->asal_sekolah,
            'jenis_kelamin' => $request->jenis_kelamin,
            'tanggal_lahir' => $request->tanggal_lahir,
            'no_telepon' => $request->no_telepon,
            'alamat' => $request->alamat,
            'dibuat_oleh' => $request->dibuat_oleh,
        );

        MemberMedan::whereId($request->hidden_id)->update($form_data);

        return response()->json(['success' => 'Data Update']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idm = MemberMedan::where('id', $id)->delete();
        // Check data deleted or not
        if($data = 1)
        {
            $success = true;
            $message = "Data deleted successfully";
        }
        else
        {
            $success = true;
            $message = "Data not found!";
        }
        //
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
