@extends('admin.layouts.master')

@section('title')
Admin Dashboard | Home
@endsection

@section('breadcrumb')
All Members
@endsection

@section('content')
<!-- basic table -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <button type="button" name="create_record" id="create_record" class="btn btn-primary sub-header text-white">
                    <i class="fa fa-plus"></i> Anggota
                </button>
                <br>
                <br>
                <div class="table-responsive">
                    <table id="zero_config" class="table table-hover table-bordered no-wrap">
                        <thead>
                            <tr class="text-center">
                                <th>Id. Anggota</th>
                                <th>Nama</th>
                                <th>Asal Sekolah</th>
                                <th>JK</th>
                                <th>Tgl. Lahir</th>
                                <th>No. Telp</th>
                                <th>LT 1</th>
                                <th>LT 2</th>
                                <th>LT 3</th>
                                <th>Created By</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($memberMedan as $key => $m)
                            <tr class="text-center">
                                <td>{{ $m->no_anggota }}</td>
                                <td>{{ $m->nama }}</td>
                                <td>{{ $m->asal_sekolah }}</td>
                                <td>{{ $m->jenis_kelamin }}</td>
                                <td>{{ $m->tanggal_lahir }}</td>
                                <td>{{ $m->no_telepon }}</td>
                                <td>
                                    {{ $m->created_at->format('d/m/Y') }} <a href="#" name="edit" id="{{ $m->id }}" class="badge badge-success">pass</a>
                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>{{ $m->dibuat_oleh }}</td>
                                <td>
                                    <a href="#" name="edit" id="{{ $m->id }}" class="edit"><i class="fa fa-edit text-info"></i></a>
                                    |
                                    <a href="#" onclick="deleteConfirmation({{ $m->id }})" class="delete"><i class="fa fa-trash text-danger"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>

    <!-- Modal Add -->
    <div id="formModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <span id="form_result"></span>
                <form method="post" id="sample_form" class="form-horizontal">
                @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input type="hidden" name="no_anggota" id="no_anggota" class="form-control" value="01" readonly>
                            </div>
                        </div>
                        <div class="col">
                            <div class="fomr-group">
                                <input type="hidden" name="dibuat_oleh" id="dibuat_oleh" class="form-control" value="{{ Auth::user()->name }}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Nama:</label>
                                <input type="text" name="nama" id="nama" class="form-control" required autocomplete="off" placeholder="Nama Lengkap">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Jenis Kelamin:</label>
                                <select name="jenis_kelamin" id="jenis_kelamin" class="form-control" required>
                                    <option value="">Pilih</option>
                                    <option value="Laki-laki">Laki-Laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Asal Sekolah:</label>
                                <input type="text" name="asal_sekolah" id="asal_sekolah" class="form-control" required autocomplete="off" placeholder="Asal Sekolah">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Tanggal Lahir:</label>
                                <input type="text" name="tanggal_lahir" id="tanggal_lahir" class="datepicker form-control" required autocomplete="off" placeholder="yyyy-mm-dd">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>No. Telp:</label>
                                <input type="text" name="no_telepon" id="no_telepon" class="form-control" required autocomplete="off" placeholder="08xx" onkeypress="return isNumberKey(event)">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Alamat:</label>
                                <textarea name="alamat" id="alamat" cols="10" rows="3" class="form-control" autocomplete="off" placeholder="Alamat"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group" align="center">
                                <input type="hidden" name="action" id="action" value="Add">
                                <input type="hidden" name="hidden_id" id="hidden_id">
                                <input type="submit" name="action_button" value="Add" id="action_button" class="btn btn-primary btn-block">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function (){
        $('#zero_config').DataTable();

        $('.datepicker').datepicker({
            uiLibrary: 'bootstrap',
            format: 'yyyy-mm-dd'
        });

        $('#create_record').click(function(){
            $('.modal-title').text('Tambah Anggota Baru');
            $('#action_button').val('Add');
            $('#action').val('Add');
            $('#form_result').html('');
            $('#formModal').modal('show');
        });

        $('#sample_form').on('submit', function(event){
            event.preventDefault();
            var id = $(this).attr('id');
            var action_url = '';

            if($('#action').val() == 'Add')
            {
                action_url = "{{ route('admin_medan.store') }}";
            }

            if($('#action').val() == 'Edit')
            {
                action_url = "{{ route('admin_medan.update') }}";
            }

            $.ajax({
                url: action_url,
                method: 'POST',
                data: $(this).serialize(),
                dataType: 'json',
                success: function(data)
                {
                    var html = '';
                    if(data.errors)
                    {
                        html = '<div class="alert alert-danger">';
                        for(var count = 0; count < data.errors.length; count++)
                        {
                            html += '<li><b>'+data.errors[count]+'</b></li>';
                        }
                        html += '</div>';
                    }

                    if(data.success)
                    {
                        Swal.fire({
                            toast: true,
                            position: 'top-right',
                            icon: 'success',
                            title: "Success",
                            text: "Data Updated",
                            showConfirmButton: false,
                            timer: 1500
                        }).then(function(){ 
                            location.reload();
                        });
                        $('#formModal').modal('hide');
                        $('#sample_form')[0].reset();
                    }
                }
            });
        });

        $(document).on('click', '.edit', function(){
            var id = $(this).attr('id');
            $('#form_result').html('');
            $.ajax({
                url: '/admin_medan/'+id+'/edit',
                dataType: 'json',
                success: function(data)
                {
                    $('#no_anggota').val(data.result.no_anggota);
                    $('#nama').val(data.result.nama);
                    $('#asal_sekolah').val(data.result.asal_sekolah);
                    $('#jenis_kelamin').val(data.result.jenis_kelamin);
                    $('#tanggal_lahir').val(data.result.tanggal_lahir);
                    $('#no_telepon').val(data.result.no_telepon);
                    $('#alamat').val(data.result.alamat);
                    $('#dibuat_oleh').val(data.result.dibuat_oleh);
                    $('#hidden_id').val(data.result.id);
                    $('.modal-title').text('Edit Anggota');
                    $('#action_button').val('Edit');
                    $('#action').val('Edit');
                    $('#formModal').modal('show');
                }
            });
        });
    });

    function deleteConfirmation(id)
    {
        Swal.fire({
            icon: "warning",
            title: "Hapus?",
            text: "Apakah anda sudah yakin?",
            type: "warning",
            showCancelButton: !0,
            cinfirmButtonText: "Ya, hapus!",
            cancelButtonText: "Batal",
            reverseButtons: !0
        }).then(function(event){
            if(event.value === true)
            {
                 var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                 $.ajax({
                    type: 'POST',
                    url: '/admin_medan/destroy/'+id,
                    data: { _token: CSRF_TOKEN },
                    dataType: 'JSON',
                    success: function(results){
                        if(results.success === true)
                        {
                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: 'Berhasil',
                                text: 'Data Terhapus',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(function(){ 
                                location.reload();
                            });
                        }
                        else
                        {
                            Swal.fire("Error!", results.message, "error");
                        }
                    }
                });
            }
            else
            {
                e.dismiss;
            }
        }, function(dismiss){
            return false;
        });
    }
    
// Input nomor saja
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

    return true;
}
</script>
@endsection