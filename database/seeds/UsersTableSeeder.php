<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $authorRole = Role::where('name', 'author')->first();
        $adminRole  = Role::where('name', 'admin')->first();
        // $memberRole = Role::where('name', 'member')->first();

        $author = User::create([
            'name' => 'Demo Athor',
            'email' => 'demo@demo.com',
            'password' => bcrypt('demodemo'),
        ]);

        $admin = User::create([
            'name' => 'Demo Admin',
            'email' => 'demo@admin.com',
            'password' => bcrypt('adminadmin'),
        ]);

        // $member = User::create([
        //     'name' => 'Akun Member',
        //     'email' => 'demo@member.com',
        //     'password' => bcrypt('membermember'),
        // ]);

        $author->roles()->attach($authorRole);
        $admin->roles()->attach($adminRole);
        // $member->roles()->attach($memberRole);
    }
}
