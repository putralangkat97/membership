<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberMedansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_medans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_anggota', 20);
            $table->string('nama', 100);
            $table->string('asal_sekolah', 100);
            $table->string('jenis_kelamin', 20);
            $table->date('tanggal_lahir');
            $table->text('alamat');
            $table->string('no_telepon', 20);
            $table->string('lead_train_satu')->nullable();
            $table->string('lead_train_dua')->nullable();
            $table->string('lead_train_tiga')->nullable();
            $table->string('dibuat_oleh', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_medans');
    }
}
