<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::namespace('Author')->middleware(['auth', 'auth.author'])->name('author.')->group(function(){
    Route::resource('/dashboard', 'AuthorController');
    Route::get('/semua-member', 'AuthorController@member');
    Route::get('/semua-admin', 'AuthorController@admin');
});

Route::group(['middleware' => ['auth', 'auth.admin']], function(){
    Route::resource('/admin_medan', 'Admin\MedanController');
    Route::post('/admin_medan/update', 'Admin\MedanController@update')->name('admin_medan.update');
    Route::post('/admin_medan/destroy/{id}', 'Admin\MedanController@destroy')->name('admin_medan.destroy');
});